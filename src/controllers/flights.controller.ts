import { JsonController, Get, Put, Body, Param } from 'routing-controllers'
import { FlightsService } from '../services/flights.service'

const flightsService = new FlightsService()

@JsonController('/flights')
export default class FlightsController {
    @Get('', { transformResponse: false })
    async getAll() {
        return {
            status: 200,
            data: await flightsService.getAll(),
        }
    }

    @Put('/:flightId')
    async onboardPassenger(@Body() body: any, @Param('flightId') flightId: string) {
        try {
            await flightsService.onboardPassenger(flightId, body.person_id)
            return {
                status: 200,
            }
        } catch (err: any) {
            return {
                status: 400,
                error: {
                    message: err.message
                }
            }
        }
    }
}
