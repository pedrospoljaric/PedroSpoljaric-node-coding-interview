import { Types } from 'mongoose'
import { FlightsModel } from '../models/flights.model'
import { PersonsModel } from '../models/persons.model'

export class FlightsService {
    getAll() {
        return FlightsModel.find()
    }

    async onboardPassenger(flightId: string, passengerId: string) {
        if (!Types.ObjectId.isValid(flightId)) throw new Error('FlightId is not valid')
        if (!Types.ObjectId.isValid(passengerId)) throw new Error('PassengerId is not valid')

        const flight = await FlightsModel.findById(flightId)
        if (!flight) throw new Error('Cannot find flight')

        if (flight.passengers?.includes(passengerId)) throw new Error('Passenger is already onboard')

        const passenger = await PersonsModel.findById(passengerId)
        if (!passenger) throw new Error('Cannot find passenger')

        flight.passengers.push(passengerId)
        flight.save()
    }
}
